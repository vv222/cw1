#include <iostream>

class Event
{
  public:
  std::string name;
  std::string s="Stand-up Comedy",l="Live Music",f="Film";
  int number,count;
  int cap1=200,cap2=200,cap3=300;
  std::string dataArr[10][5] ={
                               {"Live Music","Coldplay","15/02/22"},
			       {"Film","James Bond","04/02/22","2D"},
			       {"Stand-up Comedy","Kevin Hart","10/02/22"},
			       {"Live Music","Ed Sheeran","22/02/22"},
			       {"Film","Avengers: Endgame","11/02/22","2D"},
			       {"Stand-up Comedy","Vir Das","17/02/22"},
			       {"Live Music","Weeknd","29/02/22"},
			       {"Film","House of Gucci","18/02/22","2D"},
			       {"Stand-up Comedy","Jimmy Carr","24/02/22"}
  };
	    
  void add_details();
  void display_details();
};


void Event::add_details()
{
    std::cout<<"Please enter your Name and Number to continue. \nName: ";
    std::cin>>name;
    std::cout<<"Number: ";
    std::cin>>number;
    std::cout<<" \n";
}

void Event::display_details()
{
  std::cout<<"Welcome "<<name<<"! \n";
}

  
class Film : public Event
{
  std::string type,date,film;
public:
  void list_event()
  {
    for(int i=0;i<9;i++)
      {
	    if (dataArr[i][0] == f)
	      {
	      	std::cout<<" \n Event #"<<i<<"\n";
		std::cout<<f<<": "<<dataArr[i][1]<<"("<<dataArr[i][3]<<")";
		std::cout<<"\n Date: "<<dataArr[i][2];
	      }
      }
  }
  void book_event()
    {
    int total=5,tickets,t;
    int available = cap1 - total;
    if(cap1-available > 0)
    {
      std::cout<<"\n Enter Event number to buy it's tickets: ";
      std::cin>>t;
      std::cout<<"\n Enter the number of tickets you want to buy: ";
      std::cin>>tickets;
      	film = dataArr[t][1];
	type = dataArr[t][3];
	date = dataArr[t][2];
      if((cap3-available-tickets)<0)
	{
	  std::cout<<"\n Could not be booked: Not enough tickets left";
	}
      else
	{
	  total=total+tickets;
	  std::cout<<"\n                                                         Thankyou for purchasing "<<tickets;
	  std::cout<<" tickets for the event! \n";
	  std::cout<<"\nEvent Details: \n";
	  std::cout<<"Event name: "<<film<<" "<<type;
	  std::cout<<"\nEvent date: "<<date;
	}
    }
    }
    
};


class Standup : public Event
{
std::string sup,date;
public:
  void list_event()
  {
     for(int i=0;i<9;i++)
      {
	    if (dataArr[i][0] == s)
	      {
	      	std::cout<<" \n Event #"<<i<<"\n";
		std::cout<<s<<": "<<dataArr[i][1]<<"\n Date: "<<dataArr[i][2];

	      }
      }
  }

void book_event()
{
      int total=5,tickets,t;
      int available = cap2 - total;
      std::cout<<"\n Enter Event number to buy it's tickets: ";
      std::cin>>t;
      std::cout<<"\n Enter the number of tickets you want to buy: ";
      std::cin>>tickets;
      	sup = dataArr[t][1];
	date = dataArr[t][2];
      if((available-tickets)<0)
	{
	  std::cout<<"\n Could not be booked: Not enough tickets left";
	}
      else
	{
	  int y=total;
	  total=total+tickets;
	  std::cout<<"\n                                                         Thankyou for purchasing "<<tickets;
	  std::cout<<" for the event! \n";
	  std::cout<<"\nEvent Details: \n";
	  std::cout<<"Event name: "<<sup;
	  std::cout<<"\nEvent date: "<<date;
	  int n=1;
	  for(int x=y;x<total;x++)
	    {
      	      std::cout<<"\n Ticket "<<n<<" : Seat number "<<x;
	      n++;
	    }
	      
		
	}
}
  };


class Livemusic : public Event
{
public:
  std::string music,date;
  void list_event()
  {
    for(int i=0;i<9;i++)
      {
	    if (dataArr[i][0] == l)
	      {
	      	std::cout<<" \n Event #"<<i<<"\n";
		std::cout<<l<<": "<<dataArr[i][1]<<"\n Date: "<<dataArr[i][2];
	      }
      }
  }
  void book_event()
  {
    int total=5,tickets,t;
    int available = cap3 - total;
    if(cap3-available > 0)
    {
      std::cout<<"\n Enter Event number to buy it's tickets: ";
      std::cin>>t;
      std::cout<<"\n Enter the number of tickets you want to buy: ";
      std::cin>>tickets;
      	music = dataArr[t][1];
	date = dataArr[t][2];
      if((cap3-available-tickets)<0)
	{
	  std::cout<<"\n Could not be booked: Not enough tickets left";
	}
      else
	{
	  total=total+tickets;
	  std::cout<<"\n                                                         Thankyou for purchasing "<<tickets;
	  std::cout<<" for the event! \n";
	  std::cout<<"\nEvent Details: \n";
	  std::cout<<"Event name: "<<music;
	  std::cout<<"\nEvent date: "<<date;
	}
    }
  }
};


int main()
{
  Event e;
  Film f;
  Livemusic l;
  Standup s;
  e.add_details();
  e.display_details();
  int option2,x=0;
  std::cout<<"\n 1. Film \n 2. Stand-up Comedy \n 3. Live music \n 4. exit \n ";
    std::cin>>option2;
    switch(option2)
    {
    case 1:
      {
	f.list_event();
	f.book_event();
      }break;
    case 2:
      {
        s.list_event();
	s.book_event();
      }break;
    case 3:
      {
        l.list_event();
	l.book_event();
      }break;
    case 0:
      {
	std::cout<<"Thankyou for using the service";
      }
    default:
      {
	std::cout<<"Wrong input";
      }break;
    }
}


